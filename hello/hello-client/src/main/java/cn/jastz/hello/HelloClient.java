package cn.jastz.hello;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author zhiwen
 */
@FeignClient("hello")
public interface HelloClient {
    @GetMapping("hello")
    String hello();
}
